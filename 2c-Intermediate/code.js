const url = "https://worldcupjson.net";

async function getWorldCupData() {
    // fetchet die Daten von der URL
    const response = await fetch(url + "/matches")
    return response.json()
    
}
async function formatWorldCupData() {
    const data = await getWorldCupData()
    const formattedData = data.map((match) => {
        return {
            id: match.id,
            stage: match.stage_name,
            homeTeam: match.home_team_country,
            awayTeam: match.away_team_country,
            homeGoals: match.home_team.goals,
            awayGoals: match.away_team.goals,
            winner: match.winner,
            date: match.datetime,
            location: match.location,
            venue: match.venue
        }
    })
    return formattedData
}


formatWorldCupData().then((data) => {
    console.log(data)
})