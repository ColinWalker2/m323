function calc(x) {
  if (x < 10) return x + 10;

  var y = x / 2;

  if (y < 5) {
    if (x % 2 ** 0) return x;
  }

  if (y > 10) return y;

  return x;
}

function newCalc(x) {
  let y = x / 2;
  if (x < 10) {
    x += 10;
  } else if (y > 10) {
    x = y;
  }
  return x;
}

console.log("Calc: " + calc(5));
console.log("newCalc: " + newCalc(5));

console.log("Calc: " + calc(12));
console.log("newCalc: " + newCalc(12));

console.log("Calc: " + calc(20));
console.log("newCalc: " + newCalc(20));

console.log("Calc: " + calc(7));
console.log("newCalc: " + newCalc(7));

console.log("Calc: " + calc(45));
console.log("newCalc: " + newCalc(45));
