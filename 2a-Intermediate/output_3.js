function sum2(listOfNumers) {
  let sum = 0;
  for (let i = 0; i < listOfNumers.length; i++) {
    if (listOfNumers[i] != undefined)
      sum = sum + listOfNumers[i];
    }
  return sum;
}
let numbers = [1, , 3, 9, 27, 64];
console.log(sum2(numbers)); // result is 100


// Pure Function Erklärung

// Eine "Pure Function" ist eine Funktion, die isoliert läuft und nichts ausserhalb der Funktion beeinflusst. 
// Wenn eine Funktion etwas ausserhalb verändert, nennt man das "Side Effect".
// Das kann passieren, wenn eine Variable nur eine Referenz auf den Speicherort einer anderen Variable ist.
