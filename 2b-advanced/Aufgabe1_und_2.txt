Aufgabe 1:

Der Code soll für jeden string im Array den Integer Parsen (Die Zahl die als String dargestellt wird in eine Number umwandeln).
Das resultat soll in einen neuen Array ausgegeben werden.


Aufgabe 2:

Wenn direkt im Map die ParseInt Funktion aufgerufen wird, werden standardmässig der wert und der Index übergeben.
Das ParseInt als zweites Argument das Zahlensystem nutzt, wird der Index als Zahlensystem angegeben.
Darum wird NaN zurückgegeben, weil es den Wert nicht im angegebenen Zahlensystem gibt.

Damit es funktioniert, sollte ein callback genutzt werden, der spezifisch den wert als Variable weitergibt, 
damit er im ParseInt angegeben werden kann.



