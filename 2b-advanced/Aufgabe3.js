const numbersToParse = ["1", "2", "3"]

function mapNumbers(numbers) {
    return numbers.map((number) => parseInt(number))
}

console.log(mapNumbers(numbersToParse))