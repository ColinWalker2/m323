# Currying

## Erklärung

Currying ist eine Technik bei der eine Funktion, die mehrere Argumente akzeptiert in mehrere verschachtelte Funktionen umgewandelt wird, die je ein Argument akzeptieren.

### Beispiel

```javascript
// Beispiel ohne Currying
function add(x, y, z) {
  return x + y + z;
}
// Beispiel mit Currying
function curryAdd(x) {
  return function (y) {
    return function (z) {
      return x + y + z;
    };
  };
}
const curriedAdd = curryAdd(2)(3)(4);
console.log(curriedAdd);
```

Ein Grund um bei diesem Beispiel Currying zu verwenden ist die Möglichkeit, dynamisch Standardwerte zu definieren.

### Standardwerte Beispiel

```javascript
function curryAdd(x) {
  return function (y) {
    return function (z) {
      return x + y + z;
    };
  };
}
const addTwo = curryAdd(2);
console.log(addTwo(3)(5)); // Output: 10
```

AddTwo ist im oberen Beispiel die Funktion in curryAdd mit dem Parameter x als 2 definiert.
Somit ist addTwo eine Teilfunktion von curryAdd und braucht 2 weiter Argumente um den richtigen wert zurückzugeben.

## Übungsaufgabe

Schreibe eine Funktion, bei der 3 Teilnehmer aufgezählt werden.
Dafür soll Currying eingesetzt werden.
Anschliessend soll eine Variable definiert werden, die aus der vorhin erstellten Funktion besteht, bei dem aber das erste Argument dein Name ist.
