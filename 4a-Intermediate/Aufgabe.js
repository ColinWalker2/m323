const listOfStrings = [
    { string: "False", expected: false },
    { string: "This should be false", expected: false },
    { string: "Thisaswell", expected: false },
    { string: "ASantaatNASA", expected: true },
    { string: "radar", expected: true },
    { string: "Rotator", expected: true }
]

function checkPalindrome(string) {
    return string.toLowerCase().split("").reverse().join("") === string.toLowerCase()
}

function testCheckPalindrome(list) {
    list.forEach((item) => {
        if (checkPalindrome(item.string) === item.expected) { console.log("Passed") }
        else {
            console.log("Failed")
        }
    })
}
testCheckPalindrome(listOfStrings)