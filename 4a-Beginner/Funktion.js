function calculateFaculty(startNumber){
        if(startNumber > 1n){
            return BigInt(startNumber) * calculateFaculty(startNumber - 1)
        }
        return BigInt(startNumber)

}

// diese Funktion testet, ob die Funktion calculateFaculty richtig den erwarteten Wert zurückgibt
function testFaculty(num, result) {
    return calculateFaculty(num) === result
}


console.log(testFaculty(5, 120n))
console.log(testFaculty(12, 479001600n))
console.log(testFaculty(18, 6402373705728000n ))
console.log(testFaculty(20, 2432902008176640000n ))
console.log(testFaculty(64, 126886932185884164103433389335161480802865516174545192198801894375214704230400000000000000n ))
console.log(testFaculty(0, 0n))


//damit die Berechnungen mit grossen Zahlen funktionieren, musste ich die Zahlen in BigInt umwandeln