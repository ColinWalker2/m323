// Aufgabe 1
function multiply(...args) {
    const result = args.map((number) => {
        return number * 5;
    })
    return result;
}
console.log(multiply(1, 2, 3, 4, 5));

// Aufgabe 2
function multiplyCurry(base) {
    const baseNumber = base;
    return function (...args) {
        const result = args.map((number) => {
            return number * baseNumber;
        })
        return result;
    }
}
const multiplyByFive = multiplyCurry(5);
console.log(multiplyByFive(1, 2, 3, 4, 5));