function multiplyCurry(base) {
    const baseNumber = base;
    return function (...args) {
        const result = args.map((number) => {
            return number * baseNumber;
        })
        return result;
    }
}

const multiplyByFive = multiplyCurry(5);

console.log(multiplyByFive(1, 2, 3, 4, 5));