function sortNumbers(numbers) {
    const sortedNumbers = [...numbers].sort((a, b) => a - b);
    return sortedNumbers;
}

const testCases = [
    { input: [4, 2, 7, 1, 5], expected: [1, 2, 4, 5, 7] },
    { input: [10, 5, 8, 3, 1], expected: [1, 3, 5, 8, 10] },
    { input: [10, 50, 20, 15, 30], expected: [10, 15, 20, 30, 50] },
    { input: [9, 6, 3, 8, 1], expected: [1, 3, 6, 8, 9] },
    { input: [7, 4, 2, 9, 6], expected: [2, 4, 6, 7, 9] },
];

function testSortSumbers(input, expected) {
    const result = sortNumbers(input);

    if (result.join(" ") == expected.join(" ")) {
        console.log(`Test passed: ${input} => ${result}`);
    }
    else {
        console.error(`Test failed: ${originalInput} => ${result}, expected: ${expected}`);
    }
}

function runTests(values) {
    const originalValues = [...values];
    values.forEach((value) => {
        testSortSumbers(value.input, value.expected);
    });
    if (JSON.stringify(originalValues) === JSON.stringify(values)) {
        console.log("Test passed: No Side Effects introduced");
    }
    else {
        console.error("Test failed: Side Effects introduced");
    }
}

runTests(testCases);
