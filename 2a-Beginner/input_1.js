function isOdd(num = 2) {
    if(num % 2 === 0) {
        return `${num} is even`
    }
    else {
        return `${num} is odd`
    }
}

console.log(isOdd(4))
console.log(isOdd(3))
console.log(isOdd())