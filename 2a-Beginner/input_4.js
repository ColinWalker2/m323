
// first approach (Looping through all arguments using a for loop)
function getProduct() {
    let result = 1
    for (let i = 0; i < getProduct.arguments.length; i++) {
        result *= getProduct.arguments[i]
    }
    return result
}

console.log(getProduct(2,3))


// second approach (using ...args and going through them using a map)
function getProduct2(...args) {
    let result = 1
    args.map((num) => {
        result *= num
    })
    return result
}

console.log(getProduct2(2,3))