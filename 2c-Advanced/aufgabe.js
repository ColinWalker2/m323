
var grades = [1.2, 3.5, 5, 3, 4.75,,6,5.25];

function average(g){
    const sum =  g.reduce((acc, num) => {
        return acc += num
    }, 0)
    return sum / g.length
}

function max(g) {
    return g.reduce((acc, num) => {
        if(num > acc) {
            return num
        }
        return acc
    }, 0)
}

function min(g){
    return g.reduce((acc, num) => {
        if(acc > num) {
            return num
        }
        return acc
    }, max(g))
}

console.log(average(grades));
console.log(max(grades));
console.log(min(grades));