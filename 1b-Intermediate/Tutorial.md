# Verhältnis von Scope und Closure

## Was ist ein Scope

Der Scope ist der Bereich in dem eine Variable gültig ist.
Meistens wird das eine Funktion oder z.B. ein IF statement sein.

Es gibt Local Scope und Global Scope.
Eine Global Variable wird im obersten Scope definiert und kann darum in allen Child Scopes genutzt werden.

Eine Local Variable wird hingegen in dem Scope definiert, in dem sie gebraucht wird und kann nur in diesem und dessen Child Scopes genutzt werden.

### Global Scope

```javascript
const globalVarible = "Can be used everywhere in this file";
function test() {
  console.log(globalVariable); // Output: "Can be used everywhere in this file"
}
console.log(globalVariable); // Output: "Can be used everywhere in this file"
```

### Local Scope

```javascript
function test() {
  const localVariable = "Can only be used in this function";
  console.log(localVariable); // Output: "Can only be used in this function"
}
console.log(localVariable); // Output: "localVariable is not defined"
```

## Closure

In dem unteren Beispiel wird eine Funktion gezeigt, die bei jedem Aufrauf eine Zahl zurückgibt, die um 1 grösser ist als die beim letzten Aufruf.

Das ist möglich indem die Variable ausserhalb der Funktion definiert wird, die damit arbeitet.

Bei der Definition von counter1 wird count als 0 definiert und die innere Funktion von createCounter wird zu counter1 zugewiesen.
Wenn counter1 ausgeführt wird, wird also die innere Funktion von createCounter ausgeführt ohne count neu zu definieren.

Es ist also damit möglich, eine Variable kontinuierlich zu bearbeiten oder anderst zu verwenden, ohne, dass eine Globale Variable benötigt wird.

### Beispiel

```javascript
function createCounter() {
  let count = 0; // Wird in der äusseren Funktion / Scope definiert
  return function () {
    return ++count;
  };
}
const counter1 = createCounter();
console.log(counter1()); // 1
console.log(counter1()); // 2
```
