function foo(params) {
  var name = params[0];
  var age = params[1];
  var args = params.slice(2);
}

foo(["Name", 58]);

function foo2(name, age) {
  // any logic
}

foo2("Name", 58);

function foo3({ name, age }) {
  // any logic
}

foo3({
  name: "Name",
  age: 58,
});

function foo4(name, ...data) {
  // any logic
  console.log(name);
  console.log(data[0]);
}

foo4("Name", 58, -20);
