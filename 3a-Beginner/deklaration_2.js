- Welche Variante bevorzugen Sie? Begründen Sie Ihre Wahl.
    Ich würde die 2. Variante verwenden, weil sie am übersichtlichsten ist und man Fehleingaben beim Debuggen schneller erkennt.

- Wie heissen/Was sind die Datentypen für die Parameterübergabe in den 4 Beispielen?
    1. Array
    2. (String, number)
    3. Object
    4. (String, Array) 
    
- Was sind Named Arguments?
    Argumente, die mit einem Key versehen sind und damit aufgerufen werden.

- Spielt die Reihenfolge der Argumente bei der Definition und beim Aufruf eine Rolle? Gibt es ein
  Variante in welcher Ja und einer anderen Nein?
    Bei Variante 3 spielt es keine Rolle, weil die Argumente durch den Key aufgerufen werden und nicht durch ihre Position.
    Bei den restlichen Varianten ist es wichtig, dass man die Argumente in der gleichen Reihenfolge, wei die Parameter aufschreibt.