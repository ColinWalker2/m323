const people_1 = [
  { name: "Rafael", age: 3 },
  { name: "Juan", age: 12 },
  { name: "Robert", age: 23 },
];
// filtert alle Personen aus dem Array, die mindestens 18 Jahre alt sind
function checkMinumumAge(people, age) {
  return people.filter((person) => {
    return person.age >= age;
  });
}

const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
// filtert alle geraden Zahlen aus dem Array
function filterEvenNumbers(numbers) {
  return numbers.filter((number) => {
    return number % 2 === 0;
  });
}

console.log(checkMinumumAge(people_1, 18));
console.log(filterEvenNumbers(numbers));

// Erklärung
// Die Filter() Funktion erstellt einen neuen Array mit den Daten, die den Anforderungen, die in der Filter() Funktion geprüft werden, entsprechen.
// Es wird genutzt um Daten in einem Array zu verarbeiten und die verarbeiteten Daten in einen neuen Array zu schreiben
// Im gegensatz zu der Map() Funktion werden aber keine Daten verändert, sondern wie der Name schon sagt, gefiltert.
