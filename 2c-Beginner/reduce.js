const products1 = [
  { name: "Monster Ultra White", price: 1.7, inStock: true },
  { name: "Red-Bull Sugarfree", price: 1.7, inStock: false },
  { name: "Gipfeli", price: 3.5, inStock: true },
];
// Berechnet den Gesamtpreis aller Produkte und returnt diesen
function calculateTotalPrice(products) {
  return products.reduce((total, product) => {
    return total + product.price;
  }, 0);
}

// Zählt alle Produkte, die auf Lager sind und returnt diese
function countAvailableProducts(products) {
  return products.reduce((total, product) => {
    if (product.inStock) {
      return total + 1;
    } else {
      return total;
    }
  }, 0);
}

console.log(calculateTotalPrice(products1));
console.log(countAvailableProducts(products1));

// Erklärung
// Die Reduce() Funktion returnt einen einzigen Wert aus einem Array.
// Es wird ein Accumulator benötigt, der den Wert des vorherigen Durchlaufs speichert.
// Der Accumulator wird in der Reduce() Funktion nach dem Komma angegeben.
// Die Reduce() Funktion geht jeden Wert eines Arrays durch und modifiziert den Accumulator mit dem Wert des aktuellen Durchlaufs.
// Wie der Accumulator modifiziert wird kann mit Konditionen gesteuert werden
// Am Ende wird der Accumulator returnt.
