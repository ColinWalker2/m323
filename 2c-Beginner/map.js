function doubleNumbers(array) {
  return array.map((item) => {
    return item * 2;
  });
}

function addPrefix(array, prefix) {
  return array.map((item) => {
    if (item.startsWith(prefix)) {
      return item;
    } else {
      return prefix + item;
    }
  });
}

console.log(doubleNumbers([2, 5, 100]));
console.log(addPrefix(["Rafael", "Juan", "Hello Robert"], "Hello "));

// Erklärung
// Die Map() Funktion erstellt einen neuen Array mit den Ergebnissen die in der Map() Funktion returnt werden.
// Es wird genutzt um Daten in einem Array zu verarbeiten und die verarbeiteten Daten in einen neuen Array zu schreiben
