const fs = require('fs');

function words(str) {
    return String( str )
        .toLowerCase()
        .split( /\s|\b/ )
        .filter( function alpha(v){
            return /^[\w]+$/.test( v );
        } );
}

function unique(list) {
    var uniqList = [];

    for (let v of list) {
        // value not yet in the new list?
        if (uniqList.indexOf( v ) === -1 ) {
            uniqList.push( v );
        }
    }

    return uniqList;
}

fs.readFile(__dirname + "/text.txt", "utf-8", (err, text) => {
    if (err) {
        console.error(err);
        return;
    }
    const wordsFound = words( text );
    const wordsUsed = unique( wordsFound );
    console.log(wordsUsed);
});







