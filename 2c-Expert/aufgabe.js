const fs = require('fs')

const loadData = (file) => {
    const fileContent = fs.readFileSync(file, "utf-8")
    return fileContent.split("\n")
}


const printData = (file, count) => {
    const data = loadData(file)
    let index = 0;
    data.map((char) => {
        if(index < count) {
            console.log(char)
        }
        index++
    })
    if(count > data.length) console.log("no more data")
    console.log("")

}


printData("data.txt", 5)
printData("data.txt", 2)
printData("data.txt", 12)