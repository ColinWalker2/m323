 /*
 
 // Funktion wird in eine Variable gespeichert
 var adder = function (number1, number2) {
    return number1 + number2;
}

adder(1, 6); // Aufruf der Funktion

// Funktion in Funktion
function forEach(list, fn) {
    for (let v of list) {
        fn( v ); // hier wird die als argument übergebene Funktion aufgerufen
    }
}

forEach( [1,2,3,4,5], function each(val){
    console.log( val );
} );

// Die Konsolenausgabe ist: 1 2 3 4 5


function outerFunction() {
    return callerFunction(function innerFunction(msg){
        return msg.toUpperCase();
    });
}

function callerFunction(func) {
    return func("gibz");
}

outerFunction(); */



//Beipiel 1

function doubleNumbers(number) {
    return number * 2
}

function logNumber(number) {
    console.log(number)
}

function logOnlyNumbersDivisibleByFour(number) {
    if (number % 4 === 0) {
        logNumber(number)
    }
}

function doSomethingWithNumbers(numbers, numberProcessor, resultFunction) {
    for (let i = 0; i < numbers.length; i++) {
        resultFunction(numberProcessor(numbers[i]))
    }
}

doSomethingWithNumbers([1, 2, 3, 4, 5], doubleNumbers, logOnlyNumbersDivisibleByFour)



// Beispiel 2 

function createMultiplier(multiplier) {
    return function (number) {
        return number * multiplier;
    };
}

function logResult(number) {
    console.log(`Result: ${number}`);
}

function processNumbers(numbers, multiplier, resultFunction) {
    const customMultiplier = createMultiplier(multiplier);

    for (let i = 0; i < numbers.length; i++) {
        resultFunction(customMultiplier(numbers[i]));
    }
}

processNumbers([3, 6, 9, 12, 15], 5, logResult);


