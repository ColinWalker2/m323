// Aufgabe 1

function helloWorldFunction() {
    return function() {
        console.log("Hello World");
    }
}

const helloFunction = helloWorldFunction();
helloFunction();


// Aufgabe 2

function counter() {
    let count = 0;
    return function() {
        count++;
        return count;
    };
}

const counterFunction = counter();
console.log(counterFunction()); // 1
console.log(counterFunction()); // 2
console.log(counterFunction()); // 3


// Aufgabe 3

function adder() {
    let total = 0;
    return function(num) {
        total += num;
        return total;
    };
}

const addFunction = adder();
console.log(addFunction(5)); // 5
console.log(addFunction(3)); // 8
console.log(addFunction(10)); // 18


// Aufgabe 4

function multiplier(initialValue) {
    return function(number) {
        return initialValue * number;
    }
}

const multiplyByFive = multiplier(5);
console.log(multiplyByFive(2)); // 10
console.log(multiplyByFive(4)); // 20
console.log(multiplyByFive(10)); // 50