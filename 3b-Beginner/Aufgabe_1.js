function unary(fn) {
    return function oneArgument(arg) {
        return fn(arg);
    };
}

const numbers = [1,2,3,4,5,6,7,8,9]
const strings = ["1","2","3","4","5","6","7","8","9"]

console.log(numbers.map(unary(parseInt)));
console.log(numbers.map(parseInt));

console.log(strings.map(unary(parseInt)));
console.log(strings.map(parseInt));


// Die Map Funktion übergibt mehrere Argumente an die Parseint Funktion, die nur das erste Argument verarbeiten kann.
// Die unary Funktion sorgt dafür, dass nur ein Argument an die Parseint Funktion übergeben wird.

// Die Unary Funktion dient als zwischenschritt, um der ParseInt Funktion das richtige Argument zu übergeben.

// Zusammengefasst ist ein Unary, eine Funktion die eine Funktion als Argument akzeptiert und eine neue Funktion zurückgibt.



