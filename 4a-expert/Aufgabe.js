function fibonacci(n) {
    if (n < 2) {
        return n;
    }
    return fibonacci(n - 1) + fibonacci(n - 2);
}

function testPerformance(n) {
    const start = Date.now();
    const result = fibonacci(n);
    const end = Date.now();
    console.log(`Fibonacci of ${n} is ${result} and took ${end - start}ms`);
}

const testCases = [1, 2, 10, 15, 30, 45]

testCases.forEach((testCase) => {
    testPerformance(testCase);
})