/*

GIBZ / M323

Kompetenz 3a Advanced + Expert

Sie haben eine bestehende Anbindung an die Hunde-API: https://dog.ceo/dog-api/

Ihr Ziel ist es verschiedene Funktionen zur Verfügung zu stellen:
1. Laden aller Hunderassen
2. Laden eines Random Bildes einer Hunderasse
3. Ausgabe aller Hunderassen in der Console
4. Eine Funktion die ein Array von Hunderassen annimmt und als Ergebnis eine Liste von Random Bilder für diese Hunderassen zurückgibt
5. Eine Funktion die die Subrassen einer eingegebenen Hunderasse als Liste zurückgibt

Sie sollen dabei das gelernte Anwenden:

u.a.:

- Asynchrones Programmieren
- Keine Seiteneffekte
- Eine Funktion macht nur etwas
- Currying für Funktionen mit gleicher Basis
- Keine Code-Redundanzen
- Saubere intention
- Ihr Code sollte dokumentiert sein
- Sprechende Variablennamen
- Sinnvoller Einsatz von var/let/const

Untenstehend finden Sie den Code der Ihr Vorgänger geschrieben hat. Leider hatte dieser noch nicht so viel Erfahrung und Sie müssen schauen,
welchen Code Sie verwenden können und was Sie ggf. neu schreiben müssen.

*/


/**
 * Aufgabe: Diese Funkion sollte alle Hunderassen laden
 */

// Diese Funktion gibt einen Array von Hunderassen zurück
async function getDogBreeds() {
    const url = "https://dog.ceo/api/breeds/list/all";
    const response = await (await fetch(url)).json()
    return Object.keys(await response.message);
}

/**
 * Aufgabe: Diese Funktion sollte eine Random Bild-URL einer Hunderasse zurückgeben
 * 
 * @param {*} breed Hunderasse
 */

// Diese Funktion gibt ein zufälliges Bild der angegebenen Hunderasse zurück
async function getRandomBreedImage(breed){
    const url = "https://dog.ceo/api/breed/" + breed + "/images/random";
    const response = await (await fetch(url)).json()
    return response.message;
}

// Diese Funktion gibt einen Array von zufälligen Bild-URLs der angegebenen Hunderassen zurück
async function getRandomBreedImages(breeds){
    const images = Promise.all(breeds.map(async(breed) => {
        return await getRandomBreedImage(breed)
    }))
    return images;
}


// Aufgabe: Diese Ausgabe soll die Subrassen der angegebenen Hunderasse ausgeben.

async function getRandomSubBreed(breed) {
    const url = "https://dog.ceo/api/breed/" + breed + "/list";
    const response = await (await fetch(url)).json()
    return response.message;
}


// Aufgabe: Diese Ausgabe sollte die Anzahl Hunderassen ausgeben.

// loggt die Anzahl Hunderassen in der Console
(async () => {
    const dogs = await getDogBreeds()
    console.log("There are " + dogs.length + " breeds in the database");
})();

// Loggt ein Random Bild der angegebenen Hunderasse in der Console
(async () => {
    const image = await getRandomBreedImage("affenpinscher");
    console.log(image);
})();

// Loggt ein Array von Random Bildern der angegebenen Hunderassen in der Console
(async () => {
    const images = await getRandomBreedImages(["affenpinscher", "african", "airedale"]);
    console.log(images);
})();

// Loggt die Subrassen der angegebenen Hunderasse in der Console
(async () => {
    const subBreeds = await getRandomSubBreed("bulldog");
    console.log(subBreeds);
})();

